package com.example.policypal.model;

import java.math.BigDecimal;

/**
 * Created by nsonanh on 24/05/2018
 */
public enum WardType
{
    PRIVATE(BigDecimal.valueOf(410.00)),
    A(BigDecimal.valueOf(291.00)),
    B1(BigDecimal.valueOf(266.05));

    private BigDecimal amount;
    WardType(BigDecimal amount)
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public static WardType fromName(String wardName)
    {
        for (WardType wardType : WardType.values()) {
            if (wardType.name().equalsIgnoreCase(wardName)) {
                return wardType;
            }
        }
        return null;
    }
}
