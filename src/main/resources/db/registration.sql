DROP TABLE IF EXISTS registration;

CREATE TABLE registration (
id int(11) unsigned NOT NULL AUTO_INCREMENT,
ward_type varchar(11) DEFAULT NULL,
yearly_amount numeric(19,4) DEFAULT NULL,
created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

LOCK TABLES registration WRITE;

INSERT INTO registration (id, ward_type, yearly_amount, created, updated)
VALUES
(1, 'Private', 266.0500,'2017-02-22 00:00:00','2017-02-22 00:00:00');