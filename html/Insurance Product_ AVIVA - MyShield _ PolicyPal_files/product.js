(function() {
    function e(e) {
        return e.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function t() {
        var e = localStorage.getItem("ppUserData");
        null != e && (E = JSON.parse(e))
    }

    function a() {
        N || ($("#personal-form").find(":input").each(function() {
            switch ($(this).attr("name")) {
                case "email":
                    void 0 !== E.email && $(this).val(E.email);
                    break;
                case "nric":
                    void 0 !== E.nric && $(this).val(E.nric);
                    break;
                case "birth_date":
                    void 0 === E.dateOfBirth || S || $('#personalModal input[name="birth_date"]').datepicker("update", new Date(E.dateOfBirth));
                    break;
                case "family_name":
                    void 0 !== E.surname && $(this).val(E.surname);
                    break;
                case "given_name":
                    void 0 !== E.firstName && $(this).val(E.firstName);
                    break;
                case "mobile_number":
                    void 0 !== E.contact && $(this).val(E.contact);
                    break;
                case "gender":
                    if (void 0 !== E.gender && !O) {
                        var e = {
                            male: "Male",
                            female: "Female"
                        }[E.gender];
                        $("#personal-gender").val(e)
                    }
                    break;
                case "marital_status":
                    void 0 !== E.marital_status && $("#personal-marital-status").val(E.marital_status);
                    break;
                case "citizenship":
                    void 0 !== E.citizenship && $("#personal-citizenship").val(E.citizenship);
                    break;
                case "address":
                    void 0 !== E.address && $(this).val(E.address);
                    break;
                case "postal":
                    void 0 !== E.postal_code && $(this).val(E.postal_code);
                    break;
                case "unit_no":
                    void 0 !== E.unit_no && $(this).val(E.unit_no)
            }
        }), N = !0)
    }

    function n() {
        $(".quote-form").find(":input").each(function() {
            var e = $(this).attr("name");
            "birth_date" == e ? void 0 !== E.dateOfBirth && $(".date-of-birth").datepicker("setDate", new Date(E.dateOfBirth)) : "gender" == e ? void 0 !== E.gender && ("male" == E.gender ? $("input[value=Male]").attr("checked", !0).trigger("change") : $("input[value=Female]").attr("checked", !0).trigger("change")) : "smoke" == e && void 0 !== E.smoker && ("smoker" == E.smoker ? $("input[value=Yes]").attr("checked", !0).trigger("change") : $("input[value=No]").attr("checked", !0).trigger("change"))
        }), A = !1, void 0 !== E.dateOfBirth && $(".date-of-birth").trigger("change")
    }

    function o() {
        if ($(".quote-form").find(":input").each(function() {
                var e = $(this).attr("name"),
                    t = defaultParams[e];
                if (void 0 === t || 0 == t) return !0;
                "birth_date" == e ? (void 0 !== E.dateOfBirth && (t = E.dateOfBirth), $(".date-of-birth").datepicker("setDate", new Date(t))) : "gender" == e ? (void 0 !== E.gender && (t = {
                    male: "Male",
                    female: "Female"
                }[E.gender]), $("input[value=" + t + "]").attr("checked", !0).trigger("change")) : "smoke" == e ? (void 0 !== E.smoker && (t = {
                    smoker: "Yes",
                    "non-smoker": "No"
                }[E.smoker]), $("input[value=" + t + "]").attr("checked", !0).trigger("change")) : $(this).val(t)
            }), void 0 !== defaultParams.default_premium) {
            var t = "$ " + e(defaultParams.default_premium.net_price);
            $(".price-text").text(t), $(".price-freq-text").text(" " + defaultParams.default_premium.frequency)
        } else $(".price-text").text("Proceed to get a Quote"), $(".price-freq-text").text("");
        A = !1, void 0 !== E.dateOfBirth && $(".date-of-birth").trigger("change")
    }

    function r(e) {
        $("#coverage-amount-container").html(""), $("#coverage-amount-container").append('<div class="col-md-12 col-sm-12 col-xs-12 row-centered title">COVERAGE AMOUNTS</div>'), $("#coverage-amount-container").append('<div class="ca-table-container col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10">'), $(".ca-table-container").append('<table class="ca-table" style="width:100%; margin-top: 10px;">'), $.each(e, function(e, t) {
            $(".ca-table").append("<tr><td>" + t.title.toUpperCase() + '</td><td style="text-align: right">' + t.amount + "</td></tr>")
        }), policyWordings ? $(".policy-wording").show() : $("#coverage-amount-container").append('<div class="col-md-12 col-sm-12"><hr></div>')
    }

    function s() {
        return $(".sticky--mobile").is(":visible") ? ".sticky--mobile" : ".sticky--desktop"
    }

    function d(e, t) {
        var a = !0;
        return e.find(":input").each(function() {
            t ? !$(this).val() && $(this).is(":visible") ? ($(this).parent().addClass("has-error"), a = !1) : $(this).parent().removeClass("has-error") : !$(this).val() && $(this).is(":visible") && (a = !1)
        }), a
    }

    function l(e) {
        var t = "";
        return e.find(":input").each(function() {
            if (!$(this).is(":visible") || $(this).is(":visible") && "text" != this.type && "number" != this.type && "email" != this.type) return !0;
            var e;
            switch (this.name) {
                case "birth_date":
                case "start_date":
                case "end_date":
                    e = /^\d{1,2}\/\d{1,2}\/\d{4}$/, e.test(this.value) || ($(this).parent().addClass("has-error"), t = "Invalid date format");
                    break;
                case "coverage":
                case "coverage_text":
                case "term":
                    if (e = /^\d+$/, !e.test(this.value)) {
                        $(this).parent().addClass("has-error"), t = "Invalid " + this.name;
                        break
                    }(parseInt(this.value) < parseInt(this.min) || parseInt(this.value) > parseInt(this.max)) && ($(this).parent().addClass("has-error"), t = "Number must be between " + this.min + " and " + this.max);
                    break;
                case "email":
                    e = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, e.test(this.value) || ($(this).parent().addClass("has-error"), t = "Invalid email");
                    break;
                case (this.name.match(/fam-\w*-nric/) || {}).input:
                case "nric":
                    e = /^[s|g|f|t]\d{7}[a-z]$/i, e.test(this.value) || ($(this).parent().addClass("has-error"), t = "Invalid NRIC");
                    break;
                case (this.name.match(/fam-\w*-name/) || {}).input:
                case "family_name":
                case "given_name":
                    e = /^\D{2,}$/, e.test(this.value) || ($(this).parent().addClass("has-error"), t = "Invalid name");
                    break;
                case "mobile_number":
                    e = /^[3689]\d{7}$/, e.test(this.value) || ($(this).parent().addClass("has-error"), t = "Invalid mobile number");
                    break;
                case "postal":
                    this.value.length < 3 && ($(this).parent().addClass("has-error"), t = "Invalid postal")
            }
            return "" == t && void 0
        }), t
    }

    function c() {
        t(), $(".error-container").hide();
        var e = s(),
            i = $(e + " form");
        if (F) return $(e + " .error-container").text(F), void $(e + " .error-container").show();
        if (!d(i, !0)) return $(e + " .error-container").text("Please fill in all fields."), void $(e + " .error-container").show();
        var n = l(i);
        if ("" != n) return $(e + " .error-container").text(n), void $(e + " .error-container").show();
        if (S && $("#personalModal .date-of-birth").datepicker("update", S), m(null), v(), "singlife" == insurer && "term_life_direct_term" == product) {
            if (!$(e + " .agree-tos").is(":checked")) return $(e + " .error-container").text("Please agree to the terms to proceed."), void $(e + " .error-container").show();
            var o = $("input[name=gender]:checked", i).val(),
                r = $("input[name=smoke]:checked", i).val();
            f($("#personal-form"));
            var c = $(e + " form .date-of-birth").val();
            sessionParams.birth_date = c, sessionParams.gender = o, sessionParams.smoke = r;
            return void p($.param(sessionParams))
        }
        $(".personal-form-login").hide(), $.isEmptyObject(E) ? $(".personal-form-login").show() : a(), $("#personalModal").modal("show")
    }

    function h(e) {
        var t = new Date,
            a = t.getFullYear() - e.getFullYear(),
            i = t.getMonth() - e.getMonth();
        return (i < 0 || 0 === i && t.getDate() < e.getDate()) && a--, a
    }

    function u() {
        $(".error-container").hide();
        var e = $("#personal-form");
        if (!d(e, !0)) return $("#personalModal .error-container").text("Please fill in all fields."), void $("#personalModal .error-container").show();
        var t = l(e);
        if ("" != t) return $("#personalModal .error-container").text(t), void $("#personalModal .error-container").show();
        if ("xplora" == product) {
            var a = s(),
                i = $(a + " .xplora-travel-with").val(),
                n = !1,
                o = 17,
                r = 78,
                c = "Insured's age must be between 18 to 79 years of age next birthday";
            if ("Group" == i) {
                if (o = 21, r = 70, c = "Age of insured person and their travelling companion(s) must be between 21 and 70 years", e.find(".date-of-birth").each(function() {
                        if (!$(this).is(":visible")) return !0;
                        var e = $(this).datepicker("getDate"),
                            t = h(e);
                        return t < o || t > r ? ($(this).parent().addClass("has-error"), $("#personalModal .error-container").text(c), $("#personalModal .error-container").show(), n = !0, !1) : void 0
                    }), n) return
            } else {
                var u = $('#personalModal input[name="birth_date"]').datepicker("getDate"),
                    m = h(u);
                if (m < o || m > r) return $("#personalModal .date-of-birth").parent().addClass("has-error"), $("#personalModal .error-container").text(c), void $("#personalModal .error-container").show()
            }
        }
        if (!$("#personal-form .agree-tos").is(":checked")) return $("#personalModal .error-container").text("Please agree to the terms to proceed."), void $("#personalModal .error-container").show();
        f($("#personal-form"));
        var g = $.param(sessionParams);
        $("#proceed-btn").attr("disabled", !0), $("#proceed-btn").text("Processing.."), p(g)
    }

    function p(e) {
        var t = "http://localhost:8080/api/register";
        $.post(t, e, function(e) {
            e.form_html ? (redirect_div = document.createElement("div"), redirect_div.innerHTML = e.form_html, document.getElementsByTagName("body")[0].appendChild(redirect_div), document.getElementById(e.form_id).submit(), setTimeout(function() {
                $("#personalModal").modal("hide"), $("#proceed-btn").attr("disabled", !1), $("#proceed-btn").text("Proceed")
            }, 2e3)) : e.url ? setTimeout(function() {
                $("#personalModal").modal("hide"), $("#proceed-btn").attr("disabled", !1), $("#proceed-btn").text("Proceed"), location.href = e.url
            }, 1e3) : e.sent ? ($("#personalModal").removeClass("fade").modal("hide"), $("#alertModal").modal("show"), $("#personalModal").addClass("fade"), $("#proceed-btn").attr("disabled", !1), $("#proceed-btn").text("Proceed")) : e.token && (location.href = "/payments/checkout/" + e.token)
        }).fail(function(e) {
            alert("Request has been handled"), $("#personalModal").modal("hide"), $("#proceed-btn").attr("disabled", !1), $("#proceed-btn").text("Proceed")
        })
    }

    function f(e) {
        e.find(":input").each(function() {
            return "" == this.name || (!$(this).is(":visible") && "hidden" != $(this).attr("type") || void(sessionParams[this.name] = this.value))
        })
    }

    function m(e) {
        if (e) $.each(e, function(e, t) {
            sessionParams[e] = t
        });
        else {
            var t = s();
            $(t + " form").find(":input").each(function() {
                if (!this.name || "radio" == this.type && !this.checked) return !0;
                sessionParams[this.name] && !z ? "start_date" == this.name && (sessionParams[this.name] = this.value) : sessionParams[this.name] = this.value
            })
        }
    }

    function g() {
        if (console.log("product is " + product), "Single Trip" == $("#trip_type").find(":selected").text()) switch (product) {
            case "travelprotect":
                $("#family_popover").attr("data-content", "A Family Per Trip Plan is for 1 or 2 adults travelling with any number of children. The 2 adults need not be related but each child must be legally related to either of the adults. The family must depart and return to Singapore together.");
                break;
            case "tourcare_plus":
                $("#family_popover").attr("data-content", "Maximum 2 adults traveling with no limit on the number of children. The 2 adults need not be related but the child(ren) must be related to either one of the insured adults.");
                break;
            default:
                $("#family_popover").attr("data-content", "")
        } else switch (product) {
            case "travelprotect":
                $("#family_popover").attr("data-content", "The Family Annual Multi-Trip Plan is for 2 adults that are legally married as husband and wife with any number of children legally related to one or both of the adults.");
                break;
            case "tourcare_plus":
                $("#family_popover").attr("data-content", "For an insured, legal spouse and any number of children. The child(ren) must be accompanied by either one of the insured adults for any trips made during the Period of Insurance.<br>Child insured under a Family plan: Refers to dependent child under the age of 21 years who is unmarried and unemployed or up to 25 years of age who is in continuous full-time education in a recognized institution of higher learning.");
                break;
            default:
                $("#family_popover").attr("data-content", "")
        }
    }

    function v() {
        if ("block" == $("#family-container").css("display")) {
            W > 0 ? $(".adult-container").show() : $(".adult-container").hide();
            var e = $("#child-group > div").length;
            if (P > 0 ? $(".child-container").show() : $(".child-container").hide(), e != P)
                if (e > P)
                    if (0 == P) $("#child-group .child-container").not(":first").remove(), $(".child-container").hide();
                    else {
                        var t = e - P;
                        $("#child-group .child-container").slice(-1 * t).remove()
                    } else if (e < P) {
                var a = P - e;
                for (i = 0; i < a; i++) b("open")
            }
            $("#add-adult-btn").attr("disabled", W >= Y), $("#add-child-btn").attr("disabled", P >= V)
        } else if ("block" == $("#group-container").css("display")) {
            var n = $("#group-group > div").length;
            if (L > 0 ? $(".group-container").show() : $(".group-container").hide(), n != L)
                if (n > L)
                    if (0 == L) $("#group-group .group-container").not(":first").remove(), $(".group-container").hide();
                    else {
                        var t = n - L;
                        $("#group-group .group-container").slice(-1 * t).remove()
                    } else if (n < L) {
                var a = L - n;
                for (i = 0; i < a; i++) k("open")
            }
            $("#add-group-btn").attr("disabled", L >= I)
        }
    }

    function y() {
        1 == W && $(".adult-container").show(), $(".travel-num-adults").val((W + 1).toString()), x("adult")
    }

    function w(e) {
        W > 0 && ($("#add-adult-btn").attr("disabled", !1), T(e.val()), 1 == W ? $(".adult-container").hide() : e.parent().parent().parent().remove(), W -= 1, $(".travel-num-adults").val((W + 1).toString()), x("adult"))
    }

    function b(e) {
        if (R += 1, 1 == P) $(".child-container").show();
        else {
            var t = $("#child-group .child-container:first").clone();
            $(t).find("input, select").each(function() {
                $(this).attr("name", $(this).attr("name").replace(/\d+/, R)), $(this).val("")
            }), $(t).find("button").each(function() {
                $(this).val($(this).val().replace(/\d+/, R))
            }), $("#child-group").append(t), C($("#child-group"), 1), U($('.fam-child input[name="fam-child_' + R.toString() + '-birth_date"]')), M(t)
        }
        $(".child-container").last().trigger(e), $(".travel-num-children").val(P.toString()), x("children")
    }

    function D(e) {
        P > 0 && ($("#add-child-btn").attr("disabled", !1), T(e.val()), 1 == P ? $(".child-container").hide() : e.parent().parent().parent().remove(), P -= 1, $(".travel-num-children").val(P.toString()), C($("#child-group"), 1), x("children"))
    }

    function k(e) {
        if (q += 1, 1 == L) $(".group-container").show();
        else {
            var t = $("#group-group .group-container:first").clone();
            $(t).find("input, select").each(function() {
                $(this).attr("name", $(this).attr("name").replace(/\d+/, q)), $(this).val("")
            }), $(t).find("button").each(function() {
                $(this).val($(this).val().replace(/\d+/, q))
            }), $("#group-group").append(t), C($("#group-group"), 2), U($('.fam-group input[name="fam-group_' + q.toString() + '-birth_date"]')), M(t)
        }
        $(".group-container").last().trigger(e), $(".travel-num-group").val((L + 1).toString()), x("group")
    }

    function _(e) {
        L > 0 && ($("#add-group-btn").attr("disabled", !1), T(e.val()), 1 == L ? $(".group-container").hide() : e.parent().parent().parent().remove(), L -= 1, $(".travel-num-group").val((L + 1).toString()), C($("#group-group"), 2), x("group"))
    }

    function C(e, t) {
        e.find(".fam-header").each(function(e, a) {
            var i = $(this);
            i.text(i.text().replace(/\d+/, e + t))
        })
    }

    function x(e) {
        "adult" == e ? sessionParams.num_adult = (W + 1).toString() : "children" == e ? sessionParams.num_children = P.toString() : "group" == e && (sessionParams.num_group = (L + 1).toString())
    }

    function T(e) {
        var t = new RegExp(e + "-*");
        for (var a in sessionParams) t.test(a) && delete sessionParams[a]
    }

    function M(e) {
        $(e).collapse({
            open: function() {
                this.slideDown(150)
            },
            close: function() {},
            clickQuery: "div.toggle"
        })
    }

    function U(e) {
        $(e).datepicker({
            format: "dd/mm/yyyy",
            endDate: new Date,
            autoclose: !0
        })
    }! function(e, t) {
        function a() {
            _ = U = C = x = T = M = Y
        }

        function i(e, t) {
            for (var a in t) t.hasOwnProperty(a) && (e[a] = t[a])
        }

        function n(e) {
            return parseFloat(e) || 0
        }

        function o() {
            O = {
                top: t.pageYOffset,
                left: t.pageXOffset
            }
        }

        function r() {
            if (t.pageXOffset != O.left) return o(), void C();
            t.pageYOffset != O.top && (o(), d())
        }

        function s(e) {
            setTimeout(function() {
                t.pageYOffset != O.top && (O.top = t.pageYOffset, d())
            }, 0)
        }

        function d() {
            for (var e = A.length - 1; e >= 0; e--) l(A[e])
        }

        function l(e) {
            if (e.inited) {
                var t = O.top <= e.limit.start ? 0 : O.top >= e.limit.end ? 2 : 1;
                e.mode != t && m(e, t)
            }
        }

        function c() {
            for (var e = A.length - 1; e >= 0; e--)
                if (A[e].inited) {
                    var t = Math.abs($(A[e].clone) - A[e].docOffsetTop),
                        a = Math.abs(A[e].parent.node.offsetHeight - A[e].parent.height);
                    if (t >= 2 || a >= 2) return !1
                }
            return !0
        }

        function h(e) {
            isNaN(parseFloat(e.computed.top)) || e.isCell || "none" == e.computed.display || (e.inited = !0, e.clone || g(e), "absolute" != e.parent.computed.position && "relative" != e.parent.computed.position && (e.parent.node.style.position = "relative"), l(e), e.parent.height = e.parent.node.offsetHeight, e.docOffsetTop = $(e.clone))
        }

        function u(e) {
            var t = !0;
            e.clone && v(e), i(e.node.style, e.css);
            for (var a = A.length - 1; a >= 0; a--)
                if (A[a].node !== e.node && A[a].parent.node === e.parent.node) {
                    t = !1;
                    break
                }
            t && (e.parent.node.style.position = e.parent.css.position), e.mode = -1
        }

        function p() {
            for (var e = A.length - 1; e >= 0; e--) h(A[e])
        }

        function f() {
            for (var e = A.length - 1; e >= 0; e--) u(A[e])
        }

        function m(e, t) {
            var a = e.node.style;
            switch (t) {
                case 0:
                    a.position = "absolute", a.left = e.offset.left + "px", a.right = e.offset.right + "px", a.top = e.offset.top + "px", a.bottom = "auto", a.width = "auto", a.marginLeft = 0, a.marginRight = 0, a.marginTop = 0;
                    break;
                case 1:
                    a.position = "fixed", a.left = e.box.left + "px", a.right = e.box.right + "px", a.top = e.css.top, a.bottom = "auto", a.width = "auto", a.marginLeft = 0, a.marginRight = 0, a.marginTop = 0;
                    break;
                case 2:
                    a.position = "absolute", a.left = e.offset.left + "px", a.right = e.offset.right + "px", a.top = "auto", a.bottom = 0, a.width = "auto", a.marginLeft = 0, a.marginRight = 0
            }
            e.mode = t
        }

        function g(e) {
            e.clone = document.createElement("div");
            var t = e.node.nextSibling || e.node,
                a = e.clone.style;
            a.height = e.height + "px", a.width = e.width + "px", a.marginTop = e.computed.marginTop, a.marginBottom = e.computed.marginBottom, a.marginLeft = e.computed.marginLeft, a.marginRight = e.computed.marginRight, a.padding = a.border = a.borderSpacing = 0, a.fontSize = "1em", a.position = "static", a.cssFloat = e.computed.cssFloat, e.node.parentNode.insertBefore(e.clone, t)
        }

        function v(e) {
            e.clone.parentNode.removeChild(e.clone), e.clone = void 0
        }

        function y(e) {
            var t = getComputedStyle(e),
                a = e.parentNode,
                i = getComputedStyle(a),
                o = e.style.position;
            e.style.position = "relative";
            var r = {
                    top: t.top,
                    marginTop: t.marginTop,
                    marginBottom: t.marginBottom,
                    marginLeft: t.marginLeft,
                    marginRight: t.marginRight,
                    cssFloat: t.cssFloat,
                    display: t.display
                },
                s = {
                    top: n(t.top),
                    marginBottom: n(t.marginBottom),
                    paddingLeft: n(t.paddingLeft),
                    paddingRight: n(t.paddingRight),
                    borderLeftWidth: n(t.borderLeftWidth),
                    borderRightWidth: n(t.borderRightWidth)
                };
            e.style.position = o;
            var d = {
                    position: e.style.position,
                    top: e.style.top,
                    bottom: e.style.bottom,
                    left: e.style.left,
                    right: e.style.right,
                    width: e.style.width,
                    marginTop: e.style.marginTop,
                    marginLeft: e.style.marginLeft,
                    marginRight: e.style.marginRight
                },
                l = w(e),
                c = w(a),
                h = {
                    node: a,
                    css: {
                        position: a.style.position
                    },
                    computed: {
                        position: i.position
                    },
                    numeric: {
                        borderLeftWidth: n(i.borderLeftWidth),
                        borderRightWidth: n(i.borderRightWidth),
                        borderTopWidth: n(i.borderTopWidth),
                        borderBottomWidth: n(i.borderBottomWidth)
                    }
                };
            return {
                node: e,
                box: {
                    left: l.win.left,
                    right: N.clientWidth - l.win.right
                },
                offset: {
                    top: l.win.top - c.win.top - h.numeric.borderTopWidth,
                    left: l.win.left - c.win.left - h.numeric.borderLeftWidth,
                    right: -l.win.right + c.win.right - h.numeric.borderRightWidth
                },
                css: d,
                isCell: "table-cell" == t.display,
                computed: r,
                numeric: s,
                width: l.win.right - l.win.left,
                height: l.win.bottom - l.win.top,
                mode: -1,
                inited: !1,
                parent: h,
                limit: {
                    start: l.doc.top - s.top,
                    end: c.doc.top + a.offsetHeight - h.numeric.borderBottomWidth - e.offsetHeight - s.top - s.marginBottom
                }
            }
        }

        function $(e) {
            for (var t = 0; e;) t += e.offsetTop, e = e.offsetParent;
            return t
        }

        function w(e) {
            var a = e.getBoundingClientRect();
            return {
                doc: {
                    top: a.top + t.pageYOffset,
                    left: a.left + t.pageXOffset
                },
                win: a
            }
        }

        function b() {
            F = setInterval(function() {
                !c() && C()
            }, 500)
        }

        function D() {
            clearInterval(F)
        }

        function k() {
            E && (document[V] ? D() : b())
        }

        function _() {
            E || (o(), p(), t.addEventListener("scroll", r), t.addEventListener("wheel", s), t.addEventListener("resize", C), t.addEventListener("orientationchange", C), e.addEventListener(I, k), b(), E = !0)
        }

        function C() {
            if (E) {
                f();
                for (var e = A.length - 1; e >= 0; e--) A[e] = y(A[e].node);
                p()
            }
        }

        function x() {
            t.removeEventListener("scroll", r), t.removeEventListener("wheel", s), t.removeEventListener("resize", C), t.removeEventListener("orientationchange", C), e.removeEventListener(I, k), D(), E = !1
        }

        function T() {
            x(), f()
        }

        function M() {
            for (T(); A.length;) A.pop()
        }

        function U(e) {
            for (var t = A.length - 1; t >= 0; t--)
                if (A[t].node === e) return;
            var a = y(e);
            A.push(a), E ? h(a) : _()
        }

        function S(e) {
            for (var t = A.length - 1; t >= 0; t--) A[t].node === e && (u(A[t]), A.splice(t, 1))
        }
        var O, F, A = [],
            E = !1,
            N = e.documentElement,
            Y = function() {},
            V = "hidden",
            I = "visibilitychange";
        void 0 !== e.webkitHidden && (V = "webkitHidden", I = "webkitvisibilitychange"), t.getComputedStyle || a();
        for (var W = ["", "-webkit-", "-moz-", "-ms-"], P = document.createElement("div"), L = W.length - 1; L >= 0; L--) {
            try {
                P.style.position = W[L] + "sticky"
            } catch (e) {}
            "" != P.style.position && a()
        }
        o(), t.Stickyfill = {
            stickies: A,
            add: U,
            remove: S,
            init: _,
            rebuild: C,
            pause: x,
            stop: T,
            kill: M
        }
    }(document, window), window.jQuery && function(e) {
            e.fn.Stickyfill = function(e) {
                return this.each(function() {
                    Stickyfill.add(this)
                }), this
            }
        }(window.jQuery),
        function(e) {
            "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
        }(function(e, t) {
            function a() {
                return new Date(Date.UTC.apply(Date, arguments))
            }

            function i() {
                var e = new Date;
                return a(e.getFullYear(), e.getMonth(), e.getDate())
            }

            function n(e, t) {
                return e.getUTCFullYear() === t.getUTCFullYear() && e.getUTCMonth() === t.getUTCMonth() && e.getUTCDate() === t.getUTCDate()
            }

            function o(a, i) {
                return function() {
                    return i !== t && e.fn.datepicker.deprecated(i), this[a].apply(this, arguments)
                }
            }

            function r(e) {
                return e && !isNaN(e.getTime())
            }

            function s(t, a) {
                function i(e, t) {
                    return t.toLowerCase()
                }
                var n, o = e(t).data(),
                    r = {},
                    s = new RegExp("^" + a.toLowerCase() + "([A-Z])");
                a = new RegExp("^" + a.toLowerCase());
                for (var d in o) a.test(d) && (n = d.replace(s, i), r[n] = o[d]);
                return r
            }

            function d(t) {
                var a = {};
                if (g[t] || (t = t.split("-")[0], g[t])) {
                    var i = g[t];
                    return e.each(m, function(e, t) {
                        t in i && (a[t] = i[t])
                    }), a
                }
            }
            var l = function() {
                    var t = {
                        get: function(e) {
                            return this.slice(e)[0]
                        },
                        contains: function(e) {
                            for (var t = e && e.valueOf(), a = 0, i = this.length; a < i; a++)
                                if (0 <= this[a].valueOf() - t && this[a].valueOf() - t < 864e5) return a;
                            return -1
                        },
                        remove: function(e) {
                            this.splice(e, 1)
                        },
                        replace: function(t) {
                            t && (e.isArray(t) || (t = [t]), this.clear(), this.push.apply(this, t))
                        },
                        clear: function() {
                            this.length = 0
                        },
                        copy: function() {
                            var e = new l;
                            return e.replace(this), e
                        }
                    };
                    return function() {
                        var a = [];
                        return a.push.apply(a, arguments), e.extend(a, t), a
                    }
                }(),
                c = function(t, a) {
                    e.data(t, "datepicker", this), this._process_options(a), this.dates = new l, this.viewDate = this.o.defaultViewDate, this.focusDate = null, this.element = e(t), this.isInput = this.element.is("input"), this.inputField = this.isInput ? this.element : this.element.find("input"), this.component = !!this.element.hasClass("date") && this.element.find(".add-on, .input-group-addon, .btn"), this.component && 0 === this.component.length && (this.component = !1), this.isInline = !this.component && this.element.is("div"), this.picker = e(v.template), this._check_template(this.o.templates.leftArrow) && this.picker.find(".prev").html(this.o.templates.leftArrow), this._check_template(this.o.templates.rightArrow) && this.picker.find(".next").html(this.o.templates.rightArrow), this._buildEvents(), this._attachEvents(), this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu"), this.o.rtl && this.picker.addClass("datepicker-rtl"), this.o.calendarWeeks && this.picker.find(".datepicker-days .datepicker-switch, thead .datepicker-title, tfoot .today, tfoot .clear").attr("colspan", function(e, t) {
                        return Number(t) + 1
                    }), this._process_options({
                        startDate: this._o.startDate,
                        endDate: this._o.endDate,
                        daysOfWeekDisabled: this.o.daysOfWeekDisabled,
                        daysOfWeekHighlighted: this.o.daysOfWeekHighlighted,
                        datesDisabled: this.o.datesDisabled
                    }), this._allow_update = !1, this.setViewMode(this.o.startView), this._allow_update = !0, this.fillDow(), this.fillMonths(), this.update(), this.isInline && this.show()
                };
            c.prototype = {
                constructor: c,
                _resolveViewName: function(t) {
                    return e.each(v.viewModes, function(a, i) {
                        if (t === a || -1 !== e.inArray(t, i.names)) return t = a, !1
                    }), t
                },
                _resolveDaysOfWeek: function(t) {
                    return e.isArray(t) || (t = t.split(/[,\s]*/)), e.map(t, Number)
                },
                _check_template: function(a) {
                    try {
                        if (a === t || "" === a) return !1;
                        if ((a.match(/[<>]/g) || []).length <= 0) return !0;
                        return e(a).length > 0
                    } catch (e) {
                        return !1
                    }
                },
                _process_options: function(t) {
                    this._o = e.extend({}, this._o, t);
                    var n = this.o = e.extend({}, this._o),
                        o = n.language;
                    g[o] || (o = o.split("-")[0], g[o] || (o = f.language)), n.language = o, n.startView = this._resolveViewName(n.startView), n.minViewMode = this._resolveViewName(n.minViewMode), n.maxViewMode = this._resolveViewName(n.maxViewMode), n.startView = Math.max(this.o.minViewMode, Math.min(this.o.maxViewMode, n.startView)), !0 !== n.multidate && (n.multidate = Number(n.multidate) || !1, !1 !== n.multidate && (n.multidate = Math.max(0, n.multidate))), n.multidateSeparator = String(n.multidateSeparator), n.weekStart %= 7, n.weekEnd = (n.weekStart + 6) % 7;
                    var r = v.parseFormat(n.format);
                    n.startDate !== -1 / 0 && (n.startDate ? n.startDate instanceof Date ? n.startDate = this._local_to_utc(this._zero_time(n.startDate)) : n.startDate = v.parseDate(n.startDate, r, n.language, n.assumeNearbyYear) : n.startDate = -1 / 0), n.endDate !== 1 / 0 && (n.endDate ? n.endDate instanceof Date ? n.endDate = this._local_to_utc(this._zero_time(n.endDate)) : n.endDate = v.parseDate(n.endDate, r, n.language, n.assumeNearbyYear) : n.endDate = 1 / 0), n.daysOfWeekDisabled = this._resolveDaysOfWeek(n.daysOfWeekDisabled || []), n.daysOfWeekHighlighted = this._resolveDaysOfWeek(n.daysOfWeekHighlighted || []), n.datesDisabled = n.datesDisabled || [], e.isArray(n.datesDisabled) || (n.datesDisabled = n.datesDisabled.split(",")), n.datesDisabled = e.map(n.datesDisabled, function(e) {
                        return v.parseDate(e, r, n.language, n.assumeNearbyYear)
                    });
                    var s = String(n.orientation).toLowerCase().split(/\s+/g),
                        d = n.orientation.toLowerCase();
                    if (s = e.grep(s, function(e) {
                            return /^auto|left|right|top|bottom$/.test(e)
                        }), n.orientation = {
                            x: "auto",
                            y: "auto"
                        }, d && "auto" !== d)
                        if (1 === s.length) switch (s[0]) {
                            case "top":
                            case "bottom":
                                n.orientation.y = s[0];
                                break;
                            case "left":
                            case "right":
                                n.orientation.x = s[0]
                        } else d = e.grep(s, function(e) {
                            return /^left|right$/.test(e)
                        }), n.orientation.x = d[0] || "auto", d = e.grep(s, function(e) {
                            return /^top|bottom$/.test(e)
                        }), n.orientation.y = d[0] || "auto";
                        else;
                    if (n.defaultViewDate instanceof Date || "string" == typeof n.defaultViewDate) n.defaultViewDate = v.parseDate(n.defaultViewDate, r, n.language, n.assumeNearbyYear);
                    else if (n.defaultViewDate) {
                        var l = n.defaultViewDate.year || (new Date).getFullYear(),
                            c = n.defaultViewDate.month || 0,
                            h = n.defaultViewDate.day || 1;
                        n.defaultViewDate = a(l, c, h)
                    } else n.defaultViewDate = i()
                },
                _events: [],
                _secondaryEvents: [],
                _applyEvents: function(e) {
                    for (var a, i, n, o = 0; o < e.length; o++) a = e[o][0], 2 === e[o].length ? (i = t, n = e[o][1]) : 3 === e[o].length && (i = e[o][1], n = e[o][2]), a.on(n, i)
                },
                _unapplyEvents: function(e) {
                    for (var a, i, n, o = 0; o < e.length; o++) a = e[o][0], 2 === e[o].length ? (n = t, i = e[o][1]) : 3 === e[o].length && (n = e[o][1], i = e[o][2]), a.off(i, n)
                },
                _buildEvents: function() {
                    var t = {
                        keyup: e.proxy(function(t) {
                            -1 === e.inArray(t.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update()
                        }, this),
                        keydown: e.proxy(this.keydown, this),
                        paste: e.proxy(this.paste, this)
                    };
                    !0 === this.o.showOnFocus && (t.focus = e.proxy(this.show, this)), this.isInput ? this._events = [
                        [this.element, t]
                    ] : this.component && this.inputField.length ? this._events = [
                        [this.inputField, t],
                        [this.component, {
                            click: e.proxy(this.show, this)
                        }]
                    ] : this._events = [
                        [this.element, {
                            click: e.proxy(this.show, this),
                            keydown: e.proxy(this.keydown, this)
                        }]
                    ], this._events.push([this.element, "*", {
                        blur: e.proxy(function(e) {
                            this._focused_from = e.target
                        }, this)
                    }], [this.element, {
                        blur: e.proxy(function(e) {
                            this._focused_from = e.target
                        }, this)
                    }]), this.o.immediateUpdates && this._events.push([this.element, {
                        "changeYear changeMonth": e.proxy(function(e) {
                            this.update(e.date)
                        }, this)
                    }]), this._secondaryEvents = [
                        [this.picker, {
                            click: e.proxy(this.click, this)
                        }],
                        [this.picker, ".prev, .next", {
                            click: e.proxy(this.navArrowsClick, this)
                        }],
                        [this.picker, ".day:not(.disabled)", {
                            click: e.proxy(this.dayCellClick, this)
                        }],
                        [e(window), {
                            resize: e.proxy(this.place, this)
                        }],
                        [e(document), {
                            "mousedown touchstart": e.proxy(function(e) {
                                this.element.is(e.target) || this.element.find(e.target).length || this.picker.is(e.target) || this.picker.find(e.target).length || this.isInline || this.hide()
                            }, this)
                        }]
                    ]
                },
                _attachEvents: function() {
                    this._detachEvents(), this._applyEvents(this._events)
                },
                _detachEvents: function() {
                    this._unapplyEvents(this._events)
                },
                _attachSecondaryEvents: function() {
                    this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents)
                },
                _detachSecondaryEvents: function() {
                    this._unapplyEvents(this._secondaryEvents)
                },
                _trigger: function(t, a) {
                    var i = a || this.dates.get(-1),
                        n = this._utc_to_local(i);
                    this.element.trigger({
                        type: t,
                        date: n,
                        viewMode: this.viewMode,
                        dates: e.map(this.dates, this._utc_to_local),
                        format: e.proxy(function(e, t) {
                            0 === arguments.length ? (e = this.dates.length - 1, t = this.o.format) : "string" == typeof e && (t = e, e = this.dates.length - 1), t = t || this.o.format;
                            var a = this.dates.get(e);
                            return v.formatDate(a, t, this.o.language)
                        }, this)
                    })
                },
                show: function() {
                    if (!(this.inputField.prop("disabled") || this.inputField.prop("readonly") && !1 === this.o.enableOnReadonly)) return this.isInline || this.picker.appendTo(this.o.container), this.place(), this.picker.show(), this._attachSecondaryEvents(), this._trigger("show"), (window.navigator.msMaxTouchPoints || "ontouchstart" in document) && this.o.disableTouchKeyboard && e(this.element).blur(), this
                },
                hide: function() {
                    return this.isInline || !this.picker.is(":visible") ? this : (this.focusDate = null, this.picker.hide().detach(), this._detachSecondaryEvents(), this.setViewMode(this.o.startView), this.o.forceParse && this.inputField.val() && this.setValue(), this._trigger("hide"), this)
                },
                destroy: function() {
                    return this.hide(), this._detachEvents(), this._detachSecondaryEvents(), this.picker.remove(), delete this.element.data().datepicker, this.isInput || delete this.element.data().date, this
                },
                paste: function(t) {
                    var a;
                    if (t.originalEvent.clipboardData && t.originalEvent.clipboardData.types && -1 !== e.inArray("text/plain", t.originalEvent.clipboardData.types)) a = t.originalEvent.clipboardData.getData("text/plain");
                    else {
                        if (!window.clipboardData) return;
                        a = window.clipboardData.getData("Text")
                    }
                    this.setDate(a), this.update(), t.preventDefault()
                },
                _utc_to_local: function(e) {
                    if (!e) return e;
                    var t = new Date(e.getTime() + 6e4 * e.getTimezoneOffset());
                    return t.getTimezoneOffset() !== e.getTimezoneOffset() && (t = new Date(e.getTime() + 6e4 * t.getTimezoneOffset())), t
                },
                _local_to_utc: function(e) {
                    return e && new Date(e.getTime() - 6e4 * e.getTimezoneOffset())
                },
                _zero_time: function(e) {
                    return e && new Date(e.getFullYear(), e.getMonth(), e.getDate())
                },
                _zero_utc_time: function(e) {
                    return e && a(e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate())
                },
                getDates: function() {
                    return e.map(this.dates, this._utc_to_local)
                },
                getUTCDates: function() {
                    return e.map(this.dates, function(e) {
                        return new Date(e)
                    })
                },
                getDate: function() {
                    return this._utc_to_local(this.getUTCDate())
                },
                getUTCDate: function() {
                    var e = this.dates.get(-1);
                    return e !== t ? new Date(e) : null
                },
                clearDates: function() {
                    this.inputField.val(""), this.update(), this._trigger("changeDate"), this.o.autoclose && this.hide()
                },
                setDates: function() {
                    var t = e.isArray(arguments[0]) ? arguments[0] : arguments;
                    return this.update.apply(this, t), this._trigger("changeDate"), this.setValue(), this
                },
                setUTCDates: function() {
                    var t = e.isArray(arguments[0]) ? arguments[0] : arguments;
                    return this.setDates.apply(this, e.map(t, this._utc_to_local)), this
                },
                setDate: o("setDates"),
                setUTCDate: o("setUTCDates"),
                remove: o("destroy", "Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead"),
                setValue: function() {
                    var e = this.getFormattedDate();
                    return this.inputField.val(e), this
                },
                getFormattedDate: function(a) {
                    a === t && (a = this.o.format);
                    var i = this.o.language;
                    return e.map(this.dates, function(e) {
                        return v.formatDate(e, a, i)
                    }).join(this.o.multidateSeparator)
                },
                getStartDate: function() {
                    return this.o.startDate
                },
                setStartDate: function(e) {
                    return this._process_options({
                        startDate: e
                    }), this.update(), this.updateNavArrows(), this
                },
                getEndDate: function() {
                    return this.o.endDate
                },
                setEndDate: function(e) {
                    return this._process_options({
                        endDate: e
                    }), this.update(), this.updateNavArrows(), this
                },
                setDaysOfWeekDisabled: function(e) {
                    return this._process_options({
                        daysOfWeekDisabled: e
                    }), this.update(), this
                },
                setDaysOfWeekHighlighted: function(e) {
                    return this._process_options({
                        daysOfWeekHighlighted: e
                    }), this.update(), this
                },
                setDatesDisabled: function(e) {
                    return this._process_options({
                        datesDisabled: e
                    }), this.update(), this
                },
                place: function() {
                    if (this.isInline) return this;
                    var t = this.picker.outerWidth(),
                        a = this.picker.outerHeight(),
                        i = e(this.o.container),
                        n = i.width(),
                        o = "body" === this.o.container ? e(document).scrollTop() : i.scrollTop(),
                        r = i.offset(),
                        s = [0];
                    this.element.parents().each(function() {
                        var t = e(this).css("z-index");
                        "auto" !== t && 0 !== Number(t) && s.push(Number(t))
                    });
                    var d = Math.max.apply(Math, s) + this.o.zIndexOffset,
                        l = this.component ? this.component.parent().offset() : this.element.offset(),
                        c = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1),
                        h = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1),
                        u = l.left - r.left,
                        p = l.top - r.top;
                    "body" !== this.o.container && (p += o), this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"),
                        "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (u -= t - h)) : l.left < 0 ? (this.picker.addClass("datepicker-orient-left"), u -= l.left - 10) : u + t > n ? (this.picker.addClass("datepicker-orient-right"), u += h - t) : this.o.rtl ? this.picker.addClass("datepicker-orient-right") : this.picker.addClass("datepicker-orient-left");
                    var f, m = this.o.orientation.y;
                    if ("auto" === m && (f = -o + p - a, m = f < 0 ? "bottom" : "top"), this.picker.addClass("datepicker-orient-" + m), "top" === m ? p -= a + parseInt(this.picker.css("padding-top")) : p += c, this.o.rtl) {
                        var g = n - (u + h);
                        this.picker.css({
                            top: p,
                            right: g,
                            zIndex: d
                        })
                    } else this.picker.css({
                        top: p,
                        left: u,
                        zIndex: d
                    });
                    return this
                },
                _allow_update: !0,
                update: function() {
                    if (!this._allow_update) return this;
                    var t = this.dates.copy(),
                        a = [],
                        i = !1;
                    return arguments.length ? (e.each(arguments, e.proxy(function(e, t) {
                        t instanceof Date && (t = this._local_to_utc(t)), a.push(t)
                    }, this)), i = !0) : (a = this.isInput ? this.element.val() : this.element.data("date") || this.inputField.val(), a = a && this.o.multidate ? a.split(this.o.multidateSeparator) : [a], delete this.element.data().date), a = e.map(a, e.proxy(function(e) {
                        return v.parseDate(e, this.o.format, this.o.language, this.o.assumeNearbyYear)
                    }, this)), a = e.grep(a, e.proxy(function(e) {
                        return !this.dateWithinRange(e) || !e
                    }, this), !0), this.dates.replace(a), this.o.updateViewDate && (this.dates.length ? this.viewDate = new Date(this.dates.get(-1)) : this.viewDate < this.o.startDate ? this.viewDate = new Date(this.o.startDate) : this.viewDate > this.o.endDate ? this.viewDate = new Date(this.o.endDate) : this.viewDate = this.o.defaultViewDate), i ? (this.setValue(), this.element.change()) : this.dates.length && String(t) !== String(this.dates) && i && (this._trigger("changeDate"), this.element.change()), !this.dates.length && t.length && (this._trigger("clearDate"), this.element.change()), this.fill(), this
                },
                fillDow: function() {
                    if (this.o.showWeekDays) {
                        var t = this.o.weekStart,
                            a = "<tr>";
                        for (this.o.calendarWeeks && (a += '<th class="cw">&#160;</th>'); t < this.o.weekStart + 7;) a += '<th class="dow', -1 !== e.inArray(t, this.o.daysOfWeekDisabled) && (a += " disabled"), a += '">' + g[this.o.language].daysMin[t++ % 7] + "</th>";
                        a += "</tr>", this.picker.find(".datepicker-days thead").append(a)
                    }
                },
                fillMonths: function() {
                    for (var e, t = this._utc_to_local(this.viewDate), a = "", i = 0; i < 12; i++) e = t && t.getMonth() === i ? " focused" : "", a += '<span class="month' + e + '">' + g[this.o.language].monthsShort[i] + "</span>";
                    this.picker.find(".datepicker-months td").html(a)
                },
                setRange: function(t) {
                    t && t.length ? this.range = e.map(t, function(e) {
                        return e.valueOf()
                    }) : delete this.range, this.fill()
                },
                getClassNames: function(t) {
                    var a = [],
                        o = this.viewDate.getUTCFullYear(),
                        r = this.viewDate.getUTCMonth(),
                        s = i();
                    return t.getUTCFullYear() < o || t.getUTCFullYear() === o && t.getUTCMonth() < r ? a.push("old") : (t.getUTCFullYear() > o || t.getUTCFullYear() === o && t.getUTCMonth() > r) && a.push("new"), this.focusDate && t.valueOf() === this.focusDate.valueOf() && a.push("focused"), this.o.todayHighlight && n(t, s) && a.push("today"), -1 !== this.dates.contains(t) && a.push("active"), this.dateWithinRange(t) || a.push("disabled"), this.dateIsDisabled(t) && a.push("disabled", "disabled-date"), -1 !== e.inArray(t.getUTCDay(), this.o.daysOfWeekHighlighted) && a.push("highlighted"), this.range && (t > this.range[0] && t < this.range[this.range.length - 1] && a.push("range"), -1 !== e.inArray(t.valueOf(), this.range) && a.push("selected"), t.valueOf() === this.range[0] && a.push("range-start"), t.valueOf() === this.range[this.range.length - 1] && a.push("range-end")), a
                },
                _fill_yearsView: function(a, i, n, o, r, s, d) {
                    for (var l, c, h, u = "", p = n / 10, f = this.picker.find(a), m = Math.floor(o / n) * n, g = m + 9 * p, v = Math.floor(this.viewDate.getFullYear() / p) * p, y = e.map(this.dates, function(e) {
                            return Math.floor(e.getUTCFullYear() / p) * p
                        }), $ = m - p; $ <= g + p; $ += p) l = [i], c = null, $ === m - p ? l.push("old") : $ === g + p && l.push("new"), -1 !== e.inArray($, y) && l.push("active"), ($ < r || $ > s) && l.push("disabled"), $ === v && l.push("focused"), d !== e.noop && (h = d(new Date($, 0, 1)), h === t ? h = {} : "boolean" == typeof h ? h = {
                        enabled: h
                    } : "string" == typeof h && (h = {
                        classes: h
                    }), !1 === h.enabled && l.push("disabled"), h.classes && (l = l.concat(h.classes.split(/\s+/))), h.tooltip && (c = h.tooltip)), u += '<span class="' + l.join(" ") + '"' + (c ? ' title="' + c + '"' : "") + ">" + $ + "</span>";
                    f.find(".datepicker-switch").text(m + "-" + g), f.find("td").html(u)
                },
                fill: function() {
                    var i, n, o = new Date(this.viewDate),
                        r = o.getUTCFullYear(),
                        s = o.getUTCMonth(),
                        d = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCFullYear() : -1 / 0,
                        l = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCMonth() : -1 / 0,
                        c = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
                        h = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
                        u = g[this.o.language].today || g.en.today || "",
                        p = g[this.o.language].clear || g.en.clear || "",
                        f = g[this.o.language].titleFormat || g.en.titleFormat;
                    if (!isNaN(r) && !isNaN(s)) {
                        this.picker.find(".datepicker-days .datepicker-switch").text(v.formatDate(o, f, this.o.language)), this.picker.find("tfoot .today").text(u).css("display", !0 === this.o.todayBtn || "linked" === this.o.todayBtn ? "table-cell" : "none"), this.picker.find("tfoot .clear").text(p).css("display", !0 === this.o.clearBtn ? "table-cell" : "none"), this.picker.find("thead .datepicker-title").text(this.o.title).css("display", "string" == typeof this.o.title && "" !== this.o.title ? "table-cell" : "none"), this.updateNavArrows(), this.fillMonths();
                        var m = a(r, s, 0),
                            y = m.getUTCDate();
                        m.setUTCDate(y - (m.getUTCDay() - this.o.weekStart + 7) % 7);
                        var $ = new Date(m);
                        m.getUTCFullYear() < 100 && $.setUTCFullYear(m.getUTCFullYear()), $.setUTCDate($.getUTCDate() + 42), $ = $.valueOf();
                        for (var w, b, D = []; m.valueOf() < $;) {
                            if ((w = m.getUTCDay()) === this.o.weekStart && (D.push("<tr>"), this.o.calendarWeeks)) {
                                var k = new Date(+m + (this.o.weekStart - w - 7) % 7 * 864e5),
                                    _ = new Date(Number(k) + (11 - k.getUTCDay()) % 7 * 864e5),
                                    C = new Date(Number(C = a(_.getUTCFullYear(), 0, 1)) + (11 - C.getUTCDay()) % 7 * 864e5),
                                    x = (_ - C) / 864e5 / 7 + 1;
                                D.push('<td class="cw">' + x + "</td>")
                            }
                            b = this.getClassNames(m), b.push("day");
                            var T = m.getUTCDate();
                            this.o.beforeShowDay !== e.noop && (n = this.o.beforeShowDay(this._utc_to_local(m)), n === t ? n = {} : "boolean" == typeof n ? n = {
                                enabled: n
                            } : "string" == typeof n && (n = {
                                classes: n
                            }), !1 === n.enabled && b.push("disabled"), n.classes && (b = b.concat(n.classes.split(/\s+/))), n.tooltip && (i = n.tooltip), n.content && (T = n.content)), b = e.isFunction(e.uniqueSort) ? e.uniqueSort(b) : e.unique(b), D.push('<td class="' + b.join(" ") + '"' + (i ? ' title="' + i + '"' : "") + ' data-date="' + m.getTime().toString() + '">' + T + "</td>"), i = null, w === this.o.weekEnd && D.push("</tr>"), m.setUTCDate(m.getUTCDate() + 1)
                        }
                        this.picker.find(".datepicker-days tbody").html(D.join(""));
                        var M = g[this.o.language].monthsTitle || g.en.monthsTitle || "Months",
                            U = this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode < 2 ? M : r).end().find("tbody span").removeClass("active");
                        if (e.each(this.dates, function(e, t) {
                                t.getUTCFullYear() === r && U.eq(t.getUTCMonth()).addClass("active")
                            }), (r < d || r > c) && U.addClass("disabled"), r === d && U.slice(0, l).addClass("disabled"), r === c && U.slice(h + 1).addClass("disabled"), this.o.beforeShowMonth !== e.noop) {
                            var S = this;
                            e.each(U, function(a, i) {
                                var n = new Date(r, a, 1),
                                    o = S.o.beforeShowMonth(n);
                                o === t ? o = {} : "boolean" == typeof o ? o = {
                                    enabled: o
                                } : "string" == typeof o && (o = {
                                    classes: o
                                }), !1 !== o.enabled || e(i).hasClass("disabled") || e(i).addClass("disabled"), o.classes && e(i).addClass(o.classes), o.tooltip && e(i).prop("title", o.tooltip)
                            })
                        }
                        this._fill_yearsView(".datepicker-years", "year", 10, r, d, c, this.o.beforeShowYear), this._fill_yearsView(".datepicker-decades", "decade", 100, r, d, c, this.o.beforeShowDecade), this._fill_yearsView(".datepicker-centuries", "century", 1e3, r, d, c, this.o.beforeShowCentury)
                    }
                },
                updateNavArrows: function() {
                    if (this._allow_update) {
                        var e, t, a = new Date(this.viewDate),
                            i = a.getUTCFullYear(),
                            n = a.getUTCMonth(),
                            o = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCFullYear() : -1 / 0,
                            r = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCMonth() : -1 / 0,
                            s = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
                            d = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
                            l = 1;
                        switch (this.viewMode) {
                            case 0:
                                e = i <= o && n <= r, t = i >= s && n >= d;
                                break;
                            case 4:
                                l *= 10;
                            case 3:
                                l *= 10;
                            case 2:
                                l *= 10;
                            case 1:
                                e = Math.floor(i / l) * l <= o, t = Math.floor(i / l) * l + l >= s
                        }
                        this.picker.find(".prev").toggleClass("disabled", e), this.picker.find(".next").toggleClass("disabled", t)
                    }
                },
                click: function(t) {
                    t.preventDefault(), t.stopPropagation();
                    var n, o, r, s;
                    n = e(t.target), n.hasClass("datepicker-switch") && this.viewMode !== this.o.maxViewMode && this.setViewMode(this.viewMode + 1), n.hasClass("today") && !n.hasClass("day") && (this.setViewMode(0), this._setDate(i(), "linked" === this.o.todayBtn ? null : "view")), n.hasClass("clear") && this.clearDates(), n.hasClass("disabled") || (n.hasClass("month") || n.hasClass("year") || n.hasClass("decade") || n.hasClass("century")) && (this.viewDate.setUTCDate(1), o = 1, 1 === this.viewMode ? (s = n.parent().find("span").index(n), r = this.viewDate.getUTCFullYear(), this.viewDate.setUTCMonth(s)) : (s = 0, r = Number(n.text()), this.viewDate.setUTCFullYear(r)), this._trigger(v.viewModes[this.viewMode - 1].e, this.viewDate), this.viewMode === this.o.minViewMode ? this._setDate(a(r, s, o)) : (this.setViewMode(this.viewMode - 1), this.fill())), this.picker.is(":visible") && this._focused_from && this._focused_from.focus(), delete this._focused_from
                },
                dayCellClick: function(t) {
                    var a = e(t.currentTarget),
                        i = a.data("date"),
                        n = new Date(i);
                    this.o.updateViewDate && (n.getUTCFullYear() !== this.viewDate.getUTCFullYear() && this._trigger("changeYear", this.viewDate), n.getUTCMonth() !== this.viewDate.getUTCMonth() && this._trigger("changeMonth", this.viewDate)), this._setDate(n)
                },
                navArrowsClick: function(t) {
                    var a = e(t.currentTarget),
                        i = a.hasClass("prev") ? -1 : 1;
                    0 !== this.viewMode && (i *= 12 * v.viewModes[this.viewMode].navStep), this.viewDate = this.moveMonth(this.viewDate, i), this._trigger(v.viewModes[this.viewMode].e, this.viewDate), this.fill()
                },
                _toggle_multidate: function(e) {
                    var t = this.dates.contains(e);
                    if (e || this.dates.clear(), -1 !== t ? (!0 === this.o.multidate || this.o.multidate > 1 || this.o.toggleActive) && this.dates.remove(t) : !1 === this.o.multidate ? (this.dates.clear(), this.dates.push(e)) : this.dates.push(e), "number" == typeof this.o.multidate)
                        for (; this.dates.length > this.o.multidate;) this.dates.remove(0)
                },
                _setDate: function(e, t) {
                    t && "date" !== t || this._toggle_multidate(e && new Date(e)), (!t && this.o.updateViewDate || "view" === t) && (this.viewDate = e && new Date(e)), this.fill(), this.setValue(), t && "view" === t || this._trigger("changeDate"), this.inputField.trigger("change"), !this.o.autoclose || t && "date" !== t || this.hide()
                },
                moveDay: function(e, t) {
                    var a = new Date(e);
                    return a.setUTCDate(e.getUTCDate() + t), a
                },
                moveWeek: function(e, t) {
                    return this.moveDay(e, 7 * t)
                },
                moveMonth: function(e, t) {
                    if (!r(e)) return this.o.defaultViewDate;
                    if (!t) return e;
                    var a, i, n = new Date(e.valueOf()),
                        o = n.getUTCDate(),
                        s = n.getUTCMonth(),
                        d = Math.abs(t);
                    if (t = t > 0 ? 1 : -1, 1 === d) i = -1 === t ? function() {
                        return n.getUTCMonth() === s
                    } : function() {
                        return n.getUTCMonth() !== a
                    }, a = s + t, n.setUTCMonth(a), a = (a + 12) % 12;
                    else {
                        for (var l = 0; l < d; l++) n = this.moveMonth(n, t);
                        a = n.getUTCMonth(), n.setUTCDate(o), i = function() {
                            return a !== n.getUTCMonth()
                        }
                    }
                    for (; i();) n.setUTCDate(--o), n.setUTCMonth(a);
                    return n
                },
                moveYear: function(e, t) {
                    return this.moveMonth(e, 12 * t)
                },
                moveAvailableDate: function(e, t, a) {
                    do {
                        if (e = this[a](e, t), !this.dateWithinRange(e)) return !1;
                        a = "moveDay"
                    } while (this.dateIsDisabled(e));
                    return e
                },
                weekOfDateIsDisabled: function(t) {
                    return -1 !== e.inArray(t.getUTCDay(), this.o.daysOfWeekDisabled)
                },
                dateIsDisabled: function(t) {
                    return this.weekOfDateIsDisabled(t) || e.grep(this.o.datesDisabled, function(e) {
                        return n(t, e)
                    }).length > 0
                },
                dateWithinRange: function(e) {
                    return e >= this.o.startDate && e <= this.o.endDate
                },
                keydown: function(e) {
                    if (!this.picker.is(":visible")) return void(40 !== e.keyCode && 27 !== e.keyCode || (this.show(), e.stopPropagation()));
                    var t, a, i = !1,
                        n = this.focusDate || this.viewDate;
                    switch (e.keyCode) {
                        case 27:
                            this.focusDate ? (this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill()) : this.hide(), e.preventDefault(), e.stopPropagation();
                            break;
                        case 37:
                        case 38:
                        case 39:
                        case 40:
                            if (!this.o.keyboardNavigation || 7 === this.o.daysOfWeekDisabled.length) break;
                            t = 37 === e.keyCode || 38 === e.keyCode ? -1 : 1, 0 === this.viewMode ? e.ctrlKey ? (a = this.moveAvailableDate(n, t, "moveYear")) && this._trigger("changeYear", this.viewDate) : e.shiftKey ? (a = this.moveAvailableDate(n, t, "moveMonth")) && this._trigger("changeMonth", this.viewDate) : 37 === e.keyCode || 39 === e.keyCode ? a = this.moveAvailableDate(n, t, "moveDay") : this.weekOfDateIsDisabled(n) || (a = this.moveAvailableDate(n, t, "moveWeek")) : 1 === this.viewMode ? (38 !== e.keyCode && 40 !== e.keyCode || (t *= 4), a = this.moveAvailableDate(n, t, "moveMonth")) : 2 === this.viewMode && (38 !== e.keyCode && 40 !== e.keyCode || (t *= 4), a = this.moveAvailableDate(n, t, "moveYear")), a && (this.focusDate = this.viewDate = a, this.setValue(), this.fill(), e.preventDefault());
                            break;
                        case 13:
                            if (!this.o.forceParse) break;
                            n = this.focusDate || this.dates.get(-1) || this.viewDate, this.o.keyboardNavigation && (this._toggle_multidate(n), i = !0), this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.setValue(), this.fill(), this.picker.is(":visible") && (e.preventDefault(), e.stopPropagation(), this.o.autoclose && this.hide());
                            break;
                        case 9:
                            this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill(), this.hide()
                    }
                    i && (this.dates.length ? this._trigger("changeDate") : this._trigger("clearDate"), this.inputField.trigger("change"))
                },
                setViewMode: function(e) {
                    this.viewMode = e, this.picker.children("div").hide().filter(".datepicker-" + v.viewModes[this.viewMode].clsName).show(), this.updateNavArrows(), this._trigger("changeViewMode", new Date(this.viewDate))
                }
            };
            var h = function(t, a) {
                e.data(t, "datepicker", this), this.element = e(t), this.inputs = e.map(a.inputs, function(e) {
                    return e.jquery ? e[0] : e
                }), delete a.inputs, this.keepEmptyValues = a.keepEmptyValues, delete a.keepEmptyValues, p.call(e(this.inputs), a).on("changeDate", e.proxy(this.dateUpdated, this)), this.pickers = e.map(this.inputs, function(t) {
                    return e.data(t, "datepicker")
                }), this.updateDates()
            };
            h.prototype = {
                updateDates: function() {
                    this.dates = e.map(this.pickers, function(e) {
                        return e.getUTCDate()
                    }), this.updateRanges()
                },
                updateRanges: function() {
                    var t = e.map(this.dates, function(e) {
                        return e.valueOf()
                    });
                    e.each(this.pickers, function(e, a) {
                        a.setRange(t)
                    })
                },
                dateUpdated: function(a) {
                    if (!this.updating) {
                        this.updating = !0;
                        var i = e.data(a.target, "datepicker");
                        if (i !== t) {
                            var n = i.getUTCDate(),
                                o = this.keepEmptyValues,
                                r = e.inArray(a.target, this.inputs),
                                s = r - 1,
                                d = r + 1,
                                l = this.inputs.length;
                            if (-1 !== r) {
                                if (e.each(this.pickers, function(e, t) {
                                        t.getUTCDate() || t !== i && o || t.setUTCDate(n)
                                    }), n < this.dates[s])
                                    for (; s >= 0 && n < this.dates[s];) this.pickers[s--].setUTCDate(n);
                                else if (n > this.dates[d])
                                    for (; d < l && n > this.dates[d];) this.pickers[d++].setUTCDate(n);
                                this.updateDates(), delete this.updating
                            }
                        }
                    }
                },
                destroy: function() {
                    e.map(this.pickers, function(e) {
                        e.destroy()
                    }), e(this.inputs).off("changeDate", this.dateUpdated), delete this.element.data().datepicker
                },
                remove: o("destroy", "Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead")
            };
            var u = e.fn.datepicker,
                p = function(a) {
                    var i = Array.apply(null, arguments);
                    i.shift();
                    var n;
                    if (this.each(function() {
                            var t = e(this),
                                o = t.data("datepicker"),
                                r = "object" == typeof a && a;
                            if (!o) {
                                var l = s(this, "date"),
                                    u = e.extend({}, f, l, r),
                                    p = d(u.language),
                                    m = e.extend({}, f, p, l, r);
                                t.hasClass("input-daterange") || m.inputs ? (e.extend(m, {
                                    inputs: m.inputs || t.find("input").toArray()
                                }), o = new h(this, m)) : o = new c(this, m), t.data("datepicker", o)
                            }
                            "string" == typeof a && "function" == typeof o[a] && (n = o[a].apply(o, i))
                        }), n === t || n instanceof c || n instanceof h) return this;
                    if (this.length > 1) throw new Error("Using only allowed for the collection of a single element (" + a + " function)");
                    return n
                };
            e.fn.datepicker = p;
            var f = e.fn.datepicker.defaults = {
                    assumeNearbyYear: !1,
                    autoclose: !1,
                    beforeShowDay: e.noop,
                    beforeShowMonth: e.noop,
                    beforeShowYear: e.noop,
                    beforeShowDecade: e.noop,
                    beforeShowCentury: e.noop,
                    calendarWeeks: !1,
                    clearBtn: !1,
                    toggleActive: !1,
                    daysOfWeekDisabled: [],
                    daysOfWeekHighlighted: [],
                    datesDisabled: [],
                    endDate: 1 / 0,
                    forceParse: !0,
                    format: "mm/dd/yyyy",
                    keepEmptyValues: !1,
                    keyboardNavigation: !0,
                    language: "en",
                    minViewMode: 0,
                    maxViewMode: 4,
                    multidate: !1,
                    multidateSeparator: ",",
                    orientation: "auto",
                    rtl: !1,
                    startDate: -1 / 0,
                    startView: 0,
                    todayBtn: !1,
                    todayHighlight: !1,
                    updateViewDate: !0,
                    weekStart: 0,
                    disableTouchKeyboard: !1,
                    enableOnReadonly: !0,
                    showOnFocus: !0,
                    zIndexOffset: 10,
                    container: "body",
                    immediateUpdates: !1,
                    title: "",
                    templates: {
                        leftArrow: "&#x00AB;",
                        rightArrow: "&#x00BB;"
                    },
                    showWeekDays: !0
                },
                m = e.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"];
            e.fn.datepicker.Constructor = c;
            var g = e.fn.datepicker.dates = {
                    en: {
                        days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                        daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                        daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                        today: "Today",
                        clear: "Clear",
                        titleFormat: "MM yyyy"
                    }
                },
                v = {
                    viewModes: [{
                        names: ["days", "month"],
                        clsName: "days",
                        e: "changeMonth"
                    }, {
                        names: ["months", "year"],
                        clsName: "months",
                        e: "changeYear",
                        navStep: 1
                    }, {
                        names: ["years", "decade"],
                        clsName: "years",
                        e: "changeDecade",
                        navStep: 10
                    }, {
                        names: ["decades", "century"],
                        clsName: "decades",
                        e: "changeCentury",
                        navStep: 100
                    }, {
                        names: ["centuries", "millennium"],
                        clsName: "centuries",
                        e: "changeMillennium",
                        navStep: 1e3
                    }],
                    validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
                    nonpunctuation: /[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,
                    parseFormat: function(e) {
                        if ("function" == typeof e.toValue && "function" == typeof e.toDisplay) return e;
                        var t = e.replace(this.validParts, "\0").split("\0"),
                            a = e.match(this.validParts);
                        if (!t || !t.length || !a || 0 === a.length) throw new Error("Invalid date format.");
                        return {
                            separators: t,
                            parts: a
                        }
                    },
                    parseDate: function(a, n, o, r) {
                        function s(e, t) {
                            return !0 === t && (t = 10), e < 100 && (e += 2e3) > (new Date).getFullYear() + t && (e -= 100), e
                        }

                        function d() {
                            var e = this.slice(0, l[p].length),
                                t = l[p].slice(0, e.length);
                            return e.toLowerCase() === t.toLowerCase()
                        }
                        if (!a) return t;
                        if (a instanceof Date) return a;
                        if ("string" == typeof n && (n = v.parseFormat(n)), n.toValue) return n.toValue(a, n, o);
                        var l, h, u, p, f, m = {
                                d: "moveDay",
                                m: "moveMonth",
                                w: "moveWeek",
                                y: "moveYear"
                            },
                            y = {
                                yesterday: "-1d",
                                today: "+0d",
                                tomorrow: "+1d"
                            };
                        if (a in y && (a = y[a]), /^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/i.test(a)) {
                            for (l = a.match(/([\-+]\d+)([dmwy])/gi), a = new Date, p = 0; p < l.length; p++) h = l[p].match(/([\-+]\d+)([dmwy])/i), u = Number(h[1]), f = m[h[2].toLowerCase()], a = c.prototype[f](a, u);
                            return c.prototype._zero_utc_time(a)
                        }
                        l = a && a.match(this.nonpunctuation) || [];
                        var $, w, b = {},
                            D = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"],
                            k = {
                                yyyy: function(e, t) {
                                    return e.setUTCFullYear(r ? s(t, r) : t)
                                },
                                m: function(e, t) {
                                    if (isNaN(e)) return e;
                                    for (t -= 1; t < 0;) t += 12;
                                    for (t %= 12, e.setUTCMonth(t); e.getUTCMonth() !== t;) e.setUTCDate(e.getUTCDate() - 1);
                                    return e
                                },
                                d: function(e, t) {
                                    return e.setUTCDate(t)
                                }
                            };
                        k.yy = k.yyyy, k.M = k.MM = k.mm = k.m, k.dd = k.d, a = i();
                        var _ = n.parts.slice();
                        if (l.length !== _.length && (_ = e(_).filter(function(t, a) {
                                return -1 !== e.inArray(a, D)
                            }).toArray()), l.length === _.length) {
                            var C;
                            for (p = 0, C = _.length; p < C; p++) {
                                if ($ = parseInt(l[p], 10), h = _[p], isNaN($)) switch (h) {
                                    case "MM":
                                        w = e(g[o].months).filter(d), $ = e.inArray(w[0], g[o].months) + 1;
                                        break;
                                    case "M":
                                        w = e(g[o].monthsShort).filter(d), $ = e.inArray(w[0], g[o].monthsShort) + 1
                                }
                                b[h] = $
                            }
                            var x, T;
                            for (p = 0; p < D.length; p++)(T = D[p]) in b && !isNaN(b[T]) && (x = new Date(a), k[T](x, b[T]), isNaN(x) || (a = x))
                        }
                        return a
                    },
                    formatDate: function(t, a, i) {
                        if (!t) return "";
                        if ("string" == typeof a && (a = v.parseFormat(a)), a.toDisplay) return a.toDisplay(t, a, i);
                        var n = {
                            d: t.getUTCDate(),
                            D: g[i].daysShort[t.getUTCDay()],
                            DD: g[i].days[t.getUTCDay()],
                            m: t.getUTCMonth() + 1,
                            M: g[i].monthsShort[t.getUTCMonth()],
                            MM: g[i].months[t.getUTCMonth()],
                            yy: t.getUTCFullYear().toString().substring(2),
                            yyyy: t.getUTCFullYear()
                        };
                        n.dd = (n.d < 10 ? "0" : "") + n.d, n.mm = (n.m < 10 ? "0" : "") + n.m, t = [];
                        for (var o = e.extend([], a.separators), r = 0, s = a.parts.length; r <= s; r++) o.length && t.push(o.shift()), t.push(n[a.parts[r]]);
                        return t.join("")
                    },
                    headTemplate: '<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev">' + f.templates.leftArrow + '</th><th colspan="5" class="datepicker-switch"></th><th class="next">' + f.templates.rightArrow + "</th></tr></thead>",
                    contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
                    footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'
                };
            v.template = '<div class="datepicker"><div class="datepicker-days"><table class="table-condensed">' + v.headTemplate + "<tbody></tbody>" + v.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + v.headTemplate + v.contTemplate + v.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + v.headTemplate + v.contTemplate + v.footTemplate + '</table></div><div class="datepicker-decades"><table class="table-condensed">' + v.headTemplate + v.contTemplate + v.footTemplate + '</table></div><div class="datepicker-centuries"><table class="table-condensed">' + v.headTemplate + v.contTemplate + v.footTemplate + "</table></div></div>", e.fn.datepicker.DPGlobal = v, e.fn.datepicker.noConflict = function() {
                return e.fn.datepicker = u, this
            }, e.fn.datepicker.version = "1.7.0", e.fn.datepicker.deprecated = function(e) {
                var t = window.console;
                t && t.warn && t.warn("DEPRECATED: " + e)
            }, e(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function(t) {
                var a = e(this);
                a.data("datepicker") || (t.preventDefault(), p.call(a, "show"))
            }), e(function() {
                p.call(e('[data-provide="datepicker-inline"]'))
            })
        }), $(".sticky").Stickyfill();
    var S = null,
        O = null,
        F = null,
        A = !1,
        E = {},
        N = !1,
        Y = 1,
        V = 10,
        I = 9,
        W = 0,
        P = 0,
        L = 0,
        B = 1,
        R = 1,
        q = 1,
        z = !1;
    $.isEmptyObject(sessionParams) && (z = !0); - 1 != navigator.userAgent.indexOf("Safari") && navigator.userAgent.indexOf("Chrome"), $(document).ready(function() {
        t(), $(".policy-wording").hide(), $(".info-popover").popover({
            html: !0
        }), $(window).keydown(function(e) {
            if (13 == e.keyCode) return e.preventDefault(), !1
        });
        var a = null;
        $(".date-of-birth").datepicker({
            startView: "years",
            format: "dd/mm/yyyy",
            endDate: new Date,
            autoclose: !0
        }).on("changeDate", function(e) {
            S = new Date(e.date.valueOf())
        }), $(".start-date").datepicker({
            format: "dd/mm/yyyy",
            startDate: new Date,
            autoclose: !0
        }).datepicker("setDate", new Date), $(".travel-start-date").datepicker({
            format: "dd/mm/yyyy",
            startDate: new Date,
            endDate: "+181d",
            autoclose: !0
        }).datepicker("setDate", new Date).on("changeDate", function(e) {
            var t = new Date(e.date.valueOf());
            $(".travel-end-date").datepicker("setStartDate", t), a && a < t && $(".travel-end-date").val("")
        }), $(".travel-end-date").datepicker({
            format: "dd/mm/yyyy",
            startDate: new Date,
            endDate: "+181d",
            autoclose: !0
        }).datepicker("setDate", new Date).on("changeDate", function(e) {
            a = new Date(e.date.valueOf())
        }), $(".travel-start-date").click(function() {
            $(".travel-start-date").datepicker("place")
        }), $(".travel-end-date").click(function() {
            $(".travel-end-date").datepicker("place")
        }), $(".start-date").click(function() {
            $(".start-date").datepicker("place")
        }), $(".date-of-birth").click(function() {
            $(".date-of-birth").datepicker("place")
        }), $(document).scroll(function() {
            $(".travel-start-date").datepicker("place"), $(".travel-end-date").datepicker("place"), $(".start-date").datepicker("place"), $(".date-of-birth").datepicker("place")
        }), $(".quote-form form").ready(function() {
            function e(e) {
                const t = selectArr[0].options[e].getAttribute("data-value").split("|");
                for (var a = 0; a < 2; ++a) selectArr[a].options[e].selected = !0, coverageArr[a].options[e].selected = !0, textArr[a].innerHTML = t[t.length - 1]
            }
            if (selectArr = document.getElementsByClassName("premium-select-control"), textArr = document.getElementsByClassName("premium-select-text"), coverageArr = document.getElementsByClassName("premium-select-coverage"), 2 === selectArr.length && 2 === textArr.length && 2 === coverageArr.length) {
                e(selectArr[0].selectedIndex);
                for (var t = 0; t < 2; ++t) {
                    const a = t;
                    selectArr[a].addEventListener("change", function(t) {
                        e(selectArr[a].selectedIndex)
                    }), coverageArr[a].addEventListener("change", function(t) {
                        e(coverageArr[a].selectedIndex)
                    })
                }
            }
        }), $(".quote-form form").change(function() {
            if ("free_term_life" != product && "life" != product && !A) {
                var t = s(),
                    a = $(t + " form");
            }
        }), $(".sticky-nav span").click(function() {
            return $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 55
            }, 500), !1
        }), $("#mobile-get-covered-btn, #sticky-get-covered-btn").click(function() {
            return $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 55
            }, 500), !1
        }), $(".sticky--desktop button").click(function() {
            c()
        }), $(".sticky--mobile button").click(function() {
            c()
        }), $("#proceed-btn").click(function() {
            u()
        }), $(".pa-plan-type").change(function() {
            "Individual" == $(this).val() ? ($(".spouse-occupation-container").hide(), $(".pa-child-container").hide()) : "Individual and Spouse" == $(this).val() ? ($(".spouse-occupation-container").show(), $(".pa-child-container").hide()) : "Individual and Child(ren)" == $(this).val() ? ($(".spouse-occupation-container").hide(), $(".pa-child-container").show()) : "Individual, Spouse and Child(ren)" == $(this).val() && ($(".spouse-occupation-container").show(), $(".pa-child-container").show())
        }), $(".etiqa-trip-type").change(function() {
            "Annual Multi-Trip" == $(this).val() ? ($(".end-date-container").hide(), $(".country-container").hide(), $(".destination-container").show(), $(".travel-end-date").val(""), $(".travel-country").val("")) : ($(".end-date-container").show(), $(".country-container").show(), $(".destination-container").hide(), $(".travel-destination").val("")), $(" .price-text").text("Fill In Form To Get Quotes"), $(".price-freq-text").text(""), $("#coverage-amount-container").html("")
        }), $(".aig-trip-type").change(function() {
            "Annual Multi-Trip" == $(this).val() ? ($(".end-date-container").hide(), $(".country-container").hide(), $(".destination-container").show(), $(".travel-end-date").val(""), $(".travel-country").val("")) : ($(".end-date-container").show(), $(".country-container").show(), $(".destination-container").hide(), $(".travel-destination").val("")), $(" .price-text").text("Fill In Form To Get Quotes"), $(".price-freq-text").text(""), $("#coverage-amount-container").html("")
        }), $(".xplora-trip-type").change(function() {
            "Annual Multi-Trip" == $(this).val() ? ($(".end-date-container").hide(), $(".country-container").hide(), $(".destination-container").show(), $(".travel-end-date").val(""), $(".travel-country").val(""), g()) : ($(".end-date-container").show(), $(".country-container").show(), $(".destination-container").hide(), $(".travel-destination").val(""), g()), $(".price-text").text("Fill In Form To Get Quotes"), $(".price-freq-text").text(""), $("#coverage-amount-container").html("")
        }), $(".xplora-travel-with").change(function() {
            "Family" == $(this).val() ? ($(".num-adults-container").show(), $(".num-children-container").show(), $(".num-group-container").hide(), $(".travel-family-info").show(), $("#family-container").show(), $("#group-container").hide(), g()) : "Group" == $(this).val() ? ($(".num-adults-container").hide(), $(".num-children-container").hide(), $(".num-group-container").show(), $(".travel-family-info").hide(), $("#family-container").hide(), $("#group-container").show()) : ($(".num-adults-container").hide(), $(".num-children-container").hide(), $(".num-group-container").hide(), $(".travel-family-info").hide(), $("#family-container").hide(), $("#group-container").hide(), g())
        }), $(".manulife-readypayout-premiumterm").change(function() {
            var e = $(".manulife-readypayout-coverage-term");
            e.html(""), "10 Years" == $(this).val() ? (e.append($("<option>", {
                value: "13 Years",
                text: "13 Years"
            })), e.append($("<option>", {
                value: "15 Years",
                text: "15 Years"
            })), e.append($("<option>", {
                value: "20 Years",
                text: "20 Years"
            }))) : "15 Years" == $(this).val() ? (e.append($("<option>", {
                value: "15 Years",
                text: "15 Years"
            })), e.append($("<option>", {
                value: "20 Years",
                text: "20 Years"
            }))) : e.append($("<option>", {
                value: "20 Years",
                text: "20 Years"
            }))
        }), $(".free-age").change(function() {
            "1" == $(this).val() ? ($(".free-married-container").hide(), $(".get-covered-btn").attr("disabled", !1)) : ($(".free-married-container").show(), $(".get-covered-btn").attr("disabled", !0))
        }), $(".free-married").change(function() {
            "1" == $(this).val() ? $(".get-covered-btn").attr("disabled", !1) : $(".get-covered-btn").attr("disabled", !0)
        }), $(".quote-gender").change(function() {
            O = !0, $("#personal-gender").val($(this).val())
        }), $("#postal-btn").click(function() {
            var e = $("#personal-postal").val(),
                t = $(this).val();
            e && $.post("/addresses", {
                postal: e,
                csrfmiddlewaretoken: t
            }, function(e) {
                var t = e.block + " " + e.street;
                $("#personal-address").val(t)
            })
        }), $("#personal-form-login-link").click(function() {
            $("#personalModal").modal("hide"), $("#login-modal").modal("show")
        }), j.css({
            color: "#00d7af",
            "text-decoration": "underline"
        }), H.css({
            color: "#333",
            "text-decoration": "none"
        }), Q.css({
            color: "#333",
            "text-decoration": "none"
        }), $.isEmptyObject(defaultParams) ? $.isEmptyObject(E) || (A = !0, n()) : (A = !0, o()), "xplora" == product ? $(".xplora-trip-type").val("Single Trip") : "singlife" == insurer && "term_life_direct_term" == product && ($.isEmptyObject(E) || ($('.quote-form input[name="email"]').val(E.email), $('.quote-form input[name="family_name"]').val(E.surname), $('.quote-form input[name="given_name"]').val(E.firstName))), "" != $("#coverage-amount-container").html().trim() && $(".policy-wording").show()
    });
    var j = $("#overviewText"),
        H = $("#coveredText"),
        Q = $("#aboutInsurerText");
    $(document).scroll(function() {
            var e = ($("#overview").offset().top, $("#covered").offset().top),
                t = $("#aboutinsurer").offset().top,
                a = .3 * $(window).height();
            $(window).scrollTop() < e - a ? (j.css({
                color: "#00d7af",
                "text-decoration": "underline"
            }), H.css({
                color: "#333",
                "text-decoration": "none"
            }), Q.css({
                color: "#333",
                "text-decoration": "none"
            })) : $(window).scrollTop() >= e - a && $(window).scrollTop() < t - a && (j.css({
                color: "#333",
                "text-decoration": "none"
            }), H.css({
                color: "#00d7af",
                "text-decoration": "underline"
            }), Q.css({
                color: "#333",
                "text-decoration": "none"
            })), ($(window).scrollTop() >= t - a || $(window).scrollTop() + $(window).height() > $(document).height() - 10) && (j.css({
                color: "#333",
                "text-decoration": "none"
            }), H.css({
                color: "#333",
                "text-decoration": "none"
            }), Q.css({
                color: "#00d7af",
                "text-decoration": "underline"
            }));
            var i = $("#mobile-quote-btn").offset().top;
            $(window).scrollTop() >= i ? 0 == $("#sticky-get-covered-btn").css("opacity") && ($("#sticky-get-covered-btn").css("opacity", 1), $("#sticky-get-covered-btn").animate({
                bottom: "3px"
            }, "slow")) : ($("#sticky-get-covered-btn").css("opacity", 0), $("#sticky-get-covered-btn").css("bottom", "-60px"))
        }), $(document).ready(function() {
            M($(".adult-container")), M($(".child-container")), M($(".group-container")), $(".travel-num-adults").change(function() {
                W = parseInt($(this).val()) - 1
            }), $(".travel-num-children").change(function() {
                P = parseInt($(this).val())
            }), $(".travel-num-group").change(function() {
                L = parseInt($(this).val()) - 1
            }), $("#add-adult-btn").click(function() {
                W += 1, B += 1, W == Y && $("#add-adult-btn").attr("disabled", !0), y()
            }), $("#adult-group").on("click", ".rm-adult", function() {
                w($(this))
            }), $("#add-child-btn").click(function() {
                P += 1, P == V && $("#add-child-btn").attr("disabled", !0), b("open")
            }), $("#child-group").on("click", ".rm-child", function() {
                D($(this))
            }), $("#add-group-btn").click(function() {
                L += 1, L == I && $("#add-group-btn").attr("disabled", !0), k("open")
            }), $("#group-group").on("click", ".rm-group", function() {
                _($(this))
            })
        }),
        function(e, t) {
            function a(t, a) {
                a = a || {};
                var i = this,
                    n = a.query || "> :even";
                e.extend(i, {
                        $el: t,
                        options: a,
                        sections: [],
                        isAccordion: a.accordion || !1,
                        db: !!a.persist && jQueryCollapseStorage(t.get(0).id)
                    }), i.states = i.db ? i.db.read() : [], i.$el.find(n).each(function() {
                        new jQueryCollapseSection(e(this), i)
                    }),
                    function(t) {
                        i.$el.on("click", "[data-collapse-summary] " + (t.options.clickQuery || ""), e.proxy(i.handleClick, t)), i.$el.bind("toggle close open", e.proxy(i.handleEvent, t))
                    }(i)
            }

            function i(t, a) {
                a.options.clickQuery || t.wrapInner('<a href="#"/>'), e.extend(this, {
                    isOpen: !1,
                    $summary: t.attr("data-collapse-summary", ""),
                    $details: t.next(),
                    options: a.options,
                    parent: a
                }), a.sections.push(this);
                var i = a.states[this._index()];
                0 === i ? this.close(!0) : this.$summary.is(".open") || 1 === i ? this.open(!0) : this.close(!0)
            }
            a.prototype = {
                handleClick: function(t, a) {
                    t.preventDefault(), a = a || "toggle";
                    for (var i = this.sections, n = i.length; n--;)
                        if (e.contains(i[n].$summary[0], t.target)) {
                            i[n][a]();
                            break
                        }
                },
                handleEvent: function(e) {
                    if (e.target == this.$el.get(0)) return this[e.type]();
                    this.handleClick(e, e.type)
                },
                open: function(e) {
                    this._change("open", e)
                },
                close: function(e) {
                    this._change("close", e)
                },
                toggle: function(e) {
                    this._change("toggle", e)
                },
                _change: function(t, a) {
                    if (isFinite(a)) return this.sections[a][t]();
                    e.each(this.sections, function(e, a) {
                        a[t]()
                    })
                }
            }, i.prototype = {
                toggle: function() {
                    this.isOpen ? this.close() : this.open()
                },
                close: function(e) {
                    this._changeState("close", e)
                },
                open: function(t) {
                    var a = this;
                    a.options.accordion && !t && e.each(a.parent.sections, function(e, t) {
                        t.close()
                    }), a._changeState("open", t)
                },
                _index: function() {
                    return e.inArray(this, this.parent.sections)
                },
                _changeState: function(t, a) {
                    var i = this;
                    i.isOpen = "open" == t, e.isFunction(i.options[t]) && !a ? i.options[t].apply(i.$details) : i.$details[i.isOpen ? "show" : "hide"](), i.$summary.toggleClass("open", "close" !== t), i.$details.attr("aria-hidden", "close" === t), i.$summary.attr("aria-expanded", "open" === t), i.$summary.trigger("open" === t ? "opened" : "closed", i), i.parent.db && i.parent.db.write(i._index(), i.isOpen)
                }
            }, e.fn.extend({
                collapse: function(t, i) {
                    return (i ? e("body").find("[data-collapse]") : e(this)).each(function() {
                        var n = i ? {} : t,
                            o = e(this).attr("data-collapse") || "";
                        e.each(o.split(" "), function(e, t) {
                            t && (n[t] = !0)
                        }), new a(e(this), n)
                    })
                }
            }), t.jQueryCollapse = a, t.jQueryCollapseSection = i, e(function() {
                e.fn.collapse(!1, !0)
            })
        }(window.jQuery, window)
}).call(this);