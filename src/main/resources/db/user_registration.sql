DROP TABLE IF EXISTS user_registration;

CREATE TABLE user_registration (
  createdAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id int(11) unsigned NOT NULL,
  registration_id int(11) unsigned NOT NULL,
  PRIMARY KEY (user_id,registration_id),
  CONSTRAINT user_registration_ibfk_1 FOREIGN KEY (user_id) REFERENCES user (id),
  CONSTRAINT user_registration_ibfk_2 FOREIGN KEY (registration_id) REFERENCES registration (id)
) ENGINE=InnoDB;

LOCK TABLES user_registration WRITE;

INSERT INTO user_registration (user_id, registration_id)
VALUES (1, 1);