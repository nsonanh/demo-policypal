package com.example.policypal.service;

import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by nsonanh on 27/05/2018
 */
public class SchedulerService {

    @Scheduled(fixedRateString = "${contract.interval:120000}", initialDelayString = "${contract.contractInitialDelay:1000}")
    public void run() {
        //code run here
    }
}
