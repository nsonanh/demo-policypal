package com.example.policypal.model;

/**
 * Created by truongnguyen on 5/26/18.
 */
public enum Occasion {
    DAILY('D'), FORTNIGHTLY('F'), MONTHLY('M'), YEARLY('Y');

    private char code;
    Occasion(char code)
    {
        this.code = code;
    }

    public char getCode()
    {
        return code;
    }

    public static Occasion fromCode(char code)
    {
        for (Occasion occasion : values()) {
            if (occasion.getCode() == code) {
                return occasion;
            }
        }
        return null;
    }
}
