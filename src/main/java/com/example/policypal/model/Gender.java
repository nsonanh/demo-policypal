package com.example.policypal.model;

/**
 * Created by nsonanh on 25/05/2018
 */
public enum Gender {
    M("Male"), F("Female");

    String gender;
    Gender(String gender)
    {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public static Gender fromGender(String gender)
    {
        for (Gender genderEnum : Gender.values()) {
            if (genderEnum.getGender().equalsIgnoreCase(gender)) {
                return genderEnum;
            }
        }
        return null;
    }
}
